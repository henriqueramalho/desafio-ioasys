import axios from 'axios';

const customAxios = axios.create();

customAxios.defaults.baseURL = 'https://empresas.ioasys.com.br/';
customAxios.defaults.headers.post.ContentType = 'application/json';



customAxios.interceptors.request.use(function (config) {
    // Do something before request is sent
    const accesToken = sessionStorage.getItem('access-token'); 
    const client = sessionStorage.getItem('client'); 
    const uid = sessionStorage.getItem('uid'); 
    
    if(accesToken && client && uid) {
        config.headers['access-token'] = accesToken;       
        config.headers['client'] = client;       
        config.headers['uid'] = uid;  
        
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});


export { customAxios};  