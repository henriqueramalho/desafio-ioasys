
const URL_AUTH = 'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in';

export function authenticateUser (login, password) {

    return new Promise((resolve, reject) => {
        const requestInfo = {
            method: 'POST',
            body: JSON.stringify({login: login, password: password}),
            headers: new Headers({
                'Content-type' : 'application/json'
            })
        };
        fetch(URL_AUTH, requestInfo)
        .then(response => {
            if(response.ok) {
                resolve(response.json());
            } 
            reject('Usuário ou senha incorretos');
        })
        .catch(response => {
            reject('Não foi possível realizar a conexão com o servidor.');
        })

    });
}