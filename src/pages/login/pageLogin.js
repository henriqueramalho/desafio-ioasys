import React from 'react';
import FormLogin from '../../components/form-login/formLogin.js'
import './pageLogin.scss';

const LoginPage = (props) => {
   
    return (
        <div className="page-login">
            <div className="page-login__container">

                <h1 className="page-login__title">BEM-VINDO AO EMPRESAS</h1>
                <div className="page-login__welcome-msg">
                    <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>                    
                </div>
                <FormLogin {...props}/>
            </div>

        </div>
    )
  
}
 
export default LoginPage;

