import React, { Component } from 'react';
import './pageSearch.scss';
import SearchBar from '../../components/search-bar/searchBar.js';
import ListAlpha from '../../components/listAlpha/listAlpha.js';
import { customAxios } from '../../services/CustomAxios';

class SearchPage extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            initialState: true,
            companies: []
        }
    }

    _searchCompany = (value) => {
       
        customAxios.get(`/api/v1/enterprises?name=${value}`)
        .then(response => {
            
            this.setState({companies: response.data.enterprises, initialState: false});
        })        
    }

    render() {

        return (
            <div className="page-search">
                <div className="page-search__container">
                    <SearchBar searchFunc={this._searchCompany} />
                    {(this.state.initialState) ? <div className="page-search__message">Clique na busca para iniciar.</div> : <ListAlpha items={this.state.companies} />}
                </div>
            </div>
        )
    }
  
}
 
export default SearchPage;

