import React, { Component } from "react";
import { Link } from 'react-router-dom';
import './pageCompany.scss';
import { ReactComponent as Arrow }  from '../../imgs/icons/left-arrow.svg';
import { customAxios } from '../../services/CustomAxios';

class CompanyPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            company: null
        }
        
    }

    componentDidMount() {
        
      
        this._getCompany(this.props.match.params.id);
    }

    _getCompany = (id) => {
       
        customAxios.get(`/api/v1/enterprises/${id}`)
        .then(response => {
            
            this.setState({company: response.data.enterprise});
        })        
    }    

    render() {
        
        const { company } = this.state ;

        return (
            <div>
                {company ? (
                    <div className="company-page__container">
                        <header className="company-page__header">
                            <div className="company-page__col-icon">
                                <Link to="/search"><Arrow className="company-page__icon" /></Link>
                            </div>
                            <h1 className="company-page__name">{company.enterprise_name}</h1>
                        </header>
            
                        <div className="company-page__body">
                
                            <div className="company-detail">

                                {company.photo > 0 ?  <img src={company.photo} alt={company.enterprise_name} /> : <div className="company-detail__fake-image">E{company.id}</div>}

                                <div className="company-detail__description">
                                    {company.description}    
                                </div>
                            </div>
                        </div>
            
                    </div>
                
                    ) : (
                        <div className="page-search__message page-search__message--no-one-results">Nenhuma empresa encontrada.</div>
                    )}
            </div>              
        )   
       
    }
}
export default CompanyPage;   