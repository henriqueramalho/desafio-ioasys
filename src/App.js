import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { createBrowserHistory } from "history";
import 'reset-css';
import './scss/commom.scss';
import './scss/form.scss';
import './scss/buttons.scss';
import LoginPage from './pages/login/pageLogin';
import SearchPage from './pages/search/pageSearch';
import CompanyPage from './pages/company/pageCompany';

const customHistory = createBrowserHistory();

function App() {
  return (
    <Router history={customHistory}>
       <Switch>
        <Route path="/signin" component={LoginPage} />
        <Route path="/search" component={SearchPage} />
        <Route path="/company/:id" component={CompanyPage} />
        <Route path="/" component={LoginPage} />

       </Switch>
      
    </Router>    
  );
}

export default App;
