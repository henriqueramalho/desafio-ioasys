import React from 'react';

const MessageError = props => {
    if(!props.show) {
        return null;
    }
    return (
    <div className="form-login__msg-error">{props.children}</div>
    )
}

export default MessageError;