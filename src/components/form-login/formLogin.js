import React, { Component } from 'react';
import TextField from '../form/InputField';
import { withRouter  } from "react-router-dom";
import PropTypes from "prop-types";
import './formLogin.scss';
import Session from "../../model/Session";
import Spinner from "../spinner/spinner.js";
import MessageError from "./messageEror";


class FormLogin extends Component {


    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            errorLogin: false,
            isLoadding: false 
        }
        this.session = new Session();
       
    }


    _submit = (e) => {
        
        e.preventDefault(); 
        this.setState({isLoadding: true});
        this.session.login(this.state.email, this.state.password)
        .then(response => {

            this.setState({errorLogin : false, isLoadding: false});    
            this.props.history.push("/search");
        })
        .catch(error => {

            this.setState({errorLogin : true});
        })
        .finally(() => {
            
            this.setState({isLoadding: false});
        })
    }

    _handleEmail = (e) => {

        this.setState({email: e.target.value});
    }

    _handlePassword = (e) => {

        this.setState({password: e.target.value});
    }

   
    render() {

        const { errorLogin } = this.state; 
        return (
            <>
                <form className="form-login" onSubmit={this._submit} noValidate>
                    <TextField type="email" label="E-mail" hasError={errorLogin} onChange={this._handleEmail}/>
                    <TextField type="password"  label="Senha" hasError={errorLogin} onChange={this._handlePassword}/>
                    <MessageError show={errorLogin}>Credenciais informadas são inválidas, tente novamente.</MessageError>               
                    <div className="form-login__wrapper-button">
                        <button type="submit" className="btn-primary btn-primary--full btn-primary--extra-large">ENTRAR</button>
                    </div>
                </form>
                <Spinner show={this.state.isLoadding}/>       
            </>

        )
    }
   
  
}

FormLogin.propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};


export default withRouter(FormLogin);



