import React from 'react';
import './spinner.scss';
import { ReactComponent as SpinnerIcon }  from '../../imgs/icons/spinner.svg';

const spinner = props => {

    if(!props.show) {
        return null;
    }
    return (
        <div className="spinner-bg">
            <SpinnerIcon/>
        </div>
    )
}
export default spinner;