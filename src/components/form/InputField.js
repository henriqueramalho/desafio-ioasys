import React from 'react';
import classNames from 'classnames';
import './InputField.scss';
import { ReactComponent as EmailIcon }  from '../../imgs/icons/ic-email.svg';
import { ReactComponent as PadlockIcon }  from '../../imgs/icons/ic-cadeado.svg';
import { ReactComponent as SearchIcon }  from '../../imgs/icons/ic-search.svg';




const Icon = ({ type, aligtoRight}) => {
    
    const classIcon = classNames({
        'input-field__icon': true,      
        'input-field__icon--alig-to-right': aligtoRight,      
    });    

    if(type === 'email') {
        return <div className={classIcon}><EmailIcon/></div>;
    }
    if(type === 'password') {
        return <div className={classIcon}><PadlockIcon/></div>;
    }
    if(type === 'search') {
        return <div className={classIcon}><SearchIcon/></div>;
    }
    return null;
}

const ErrorIcon = () => <span className="input-field__error-icon">!</span>

const eqType = type => {
    if(type === 'search') {
        return "text"
    }
    return type;
}


const TextField = props => {
    
    const { type, label, hasError } = props;
    const classInput = classNames({
        'input-field__input': true,
        'input-field__input--invalid': hasError,
        'input-field__input--white-border':  props.whiteBorder,
        'input-field__input--white-color':  props.whiteColor,
        'input-field__input--initial-hidden':  props.hiddenField        
    });    



    return (
        <div className="input-field">
            <Icon type={type} aligtoRight={props.hiddenField} />
            <label className="src-only">{label}</label>
            <input type={eqType(type)} className={classInput} placeholder={label} onChange={props.onChange}/>
            {hasError ? <ErrorIcon/> : ''}
        </div>  
    )
};

export  default TextField;