import React, { Component } from 'react';
import TextField from '../form/InputField';
import './searchBar.scss';
import Logo from './logo';

class SearchBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hiddenBar: true
        }
    }

    _handleBar = e  => {

        this.setState({hiddenBar: false});
    }

    _handleOnChange = e => {

        console.log(e.target.value);
        this.props.searchFunc(e.target.value);

        // https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=1&name=aQm
    }

    render() {
        return (
            <div className="search-bar" onClick={this._handleBar}>
                <Logo hidden={!this.state.hiddenBar} />
                <div className="search-bar__wrapper-field">
                    <TextField type="search" placeholder="Pesquisar" whiteBorder whiteColor hiddenField={this.state.hiddenBar} onChange={this._handleOnChange} />

                </div>
            </div>
        )
    }    

}    


export default SearchBar;