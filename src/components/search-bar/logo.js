import React from 'react';
import classNames from 'classnames';
import logo from '../../imgs/ioasys.png';




const Logo = props => {

    const clazz = classNames({
        'search-bar__logo': true,      
        'search-bar__logo--hidden': props.hidden,      
    });    
    
  return <img src={logo} alt="Logo" className={clazz}/>;
}
export default Logo;