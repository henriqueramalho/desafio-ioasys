import React from 'react';
import { useHistory  } from 'react-router-dom';
import './listAlpha.scss';

const List = ({ items }) => {   
    
    let history = useHistory();

    const redirecToCompany = (id) => {
        history.push(`/company/${id}`);
    }

    return ( 
        <div>
            {items.length > 0 ? (
    
                <div className="list-alpha">
                    <ul>
                        {items.map((item) => {
                            return(
                                <li key={item.id} className="list-alpha-item" onClick={redirecToCompany.bind(this, item.id)}>
                                    <div className="list-alpha-item__col-image">
                                    
                                        {item.photo > 0 ?  <img src={item.photo} alt={item.enterprise_name} /> : <div className="list-alpha-item__fake-image">E{item.id}</div>}
                                        
                                    </div>
                                    <div className="list-alpha-item__col-content">
                                        <h3 className="list-alpha-item__name">{item.enterprise_name}</h3>
                                        <h5 className="list-alpha-item__type">{item.enterprise_type.enterprise_type_name}</h5>
                                        <h6 className="list-alpha-item__country">{item.country}</h6>
                                    </div>
                                </li>
                            )   
                        })}
                    </ul>
                </div>
              ) : (
                <div className="page-search__message page-search__message--no-one-results">Nenhuma empresa foi encontrada para a busca realizada.</div>
              )}

        </div>

    )
}

export default List ;