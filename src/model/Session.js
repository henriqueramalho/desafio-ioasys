
//import axios from 'axios';
import { customAxios} from '../services/CustomAxios';
 
const SESSION_STORAGE_KEY__ACCES_TOKEN = 'access-token';
const SESSION_STORAGE_KEY__client = 'client';
const SESSION_STORAGE_KEY__uid = 'uid';

export default class Session {

    constructor() {
        this._session = undefined;
    }

    login(email, password) {

        return new Promise((resolve, reject) => {

            customAxios.post('/api/v1/users/auth/sign_in',{ email, password })
            .then(response => {
                
                if(response.headers['access-token'] && response.headers['client'] &&  response.headers['uid']) {
                    this.setDatasToAcces(response.headers['access-token'], response.headers['client'], response.headers['uid']);
                    resolve(response.data);
                }
            })
            .catch(err => reject(err));
        });

    }
    
    setDatasToAcces(accesToken, client, uid) {
        
        if(!accesToken || !client || !uid) {
            throw new Error('data info to acces not provided');
        }       
        
        sessionStorage.setItem(SESSION_STORAGE_KEY__ACCES_TOKEN, accesToken); 
        sessionStorage.setItem(SESSION_STORAGE_KEY__client, client); 
        sessionStorage.setItem(SESSION_STORAGE_KEY__uid, uid); 

    }

   

    logout() {

        sessionStorage.removeItem(SESSION_STORAGE_KEY__ACCES_TOKEN); 
        sessionStorage.removeItem(SESSION_STORAGE_KEY__client); 
        sessionStorage.removeItem(SESSION_STORAGE_KEY__uid); 
    }



}